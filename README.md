flake8-lending
==============
`lending` is a `flake8` plugin that checks code for inconsistent line-endings.

## Installation
The easiest (and recommended) way to install this plugin is using `pip`:

    $ pip install flake8-lending
    
Alternatively you can install the package directly from source:
1. Clone this repo to a location of your choosing
2. `cd` into the cloned repo
3. Run `python setup.py install`

#### Testing
To test this package:

    $ cd <my-clone-dir>
    $ tox

(You'll have to have `tox` installed of course) 


## Usage
This plugin "just works" so you don't need to do anything other than installing
it. It will be picked up automatically by `flake8`.
The next time you'll invoke `flake8` your files will also be checked for
line-endings.

### Configuration
This plugin supports setting the desired line-ending style.
The default is the `unix` style, in which lines ends with `\n` only.
This is probably the preferred style for most of the Open Source projects out
there so you should rarely need to change the desired style.

However, if you do wish to change it (please don't) you can specify the desired
style through a command-line option to `flake8`, or through a configuration
file supported by `flake8`, such as `tox.ini`, `setup.cfg` etc.

#### Command-line option
Add the `--line-ending` to `flake8` invocation, and pass in one of the three
supported values:
1. `unix` (Default)
2. `dos`
3. `mac`

For example:

    $ flake8 --line-ending=dos file.py
    
#### Configuration file
You can add the `line-ending` option in any of the configuration files
supported by `flake8`.
Usually this will be an `ini` file, with a `[flake8]` section in it.
Under that section simply add the line `line-ending = <style>`

See http://flake8.pycqa.org/en/latest/user/configuration.html for details.