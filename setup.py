import io
from os.path import join, dirname
from setuptools import setup, find_packages


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()


requirements = read("requirements.txt").splitlines()


setup(
    name='flake8-lending',
    version='0.1.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    url='https://gitlab.com/avrahamshukron/flake8-lending',
    license='MIT',
    author='Avraham Shukron',
    author_email='avraham.shukron@gmail.com',
    description='Line-ending checker plugin for flake8',
    long_description=read("README.md"),
    include_package_data=True,
    zip_safe=False,
    entry_points={
        "flake8.extension": [
            'L = flake8_lending.checker:check_line_ending',
        ],
    },
    classifiers=[
        "Framework :: Flake8",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: Quality Assurance",
    ],
    keywords=[
        "flake8", "coding-style", "lint", "line-endings", "checker"
    ],
    install_requires=requirements,
)
