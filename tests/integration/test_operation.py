import subprocess
from pytest import mark

from os.path import dirname, join

from flake8_lending.checker import LINE_ENDINGS


def aux_file(name):
    return join(dirname(__file__), "aux", name)


@mark.xfail(
    reason="This test fails due to a bug in flake8."
           "See https://gitlab.com/pycqa/flake8/issues/408")
def test_valid():
    styles = list(LINE_ENDINGS.keys())
    for style in styles:
        test_file = aux_file("{}.py".format(style))
        code = subprocess.call(
            "flake8 --line-ending={} {}".format(style, test_file), shell=True)
        assert code == 0


def test_invalid():
    styles = list(LINE_ENDINGS.keys())
    for index, style in enumerate(styles):
        wrong = styles[(index + 1) % len(styles)]
        test_file = aux_file("{}.py".format(wrong))
        handle = subprocess.Popen(
            "flake8 -vvv --line-ending={} {}".format(style, test_file),
            shell=True)

        return_code = handle.wait()
        assert return_code == 1
