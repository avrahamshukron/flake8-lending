from flake8_lending.checker import infer_ending_style, DOS, MAC, UNIX


def test_correct_detection():
    mac = "this is a mac line\r"
    unix = "this is a mac line\n"
    dos = "this is a mac line\r\n"
    none = "this line does not end well..."

    assert infer_ending_style(mac) == MAC
    assert infer_ending_style(dos) == DOS
    assert infer_ending_style(unix) == UNIX
    assert infer_ending_style(none) is None
